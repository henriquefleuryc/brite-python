import asyncio

from backend.client import omdb_transport
from backend import test
from backend.movie import Movie


class TestOmdbTransport(test.TestCase):
    
    def test_search_quantity(self):
        omdb =  omdb_transport.OmdbTransport()
        loop = asyncio.get_event_loop()
        result = []
        for page in range(1, 11):
            result.append(loop.run_until_complete(omdb.get_movies_async(page)))
        self.assertEqual(len(result), 10)
        
        