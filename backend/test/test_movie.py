from backend import test, movie

import time

class TestMovie(test.TestCase):

    def test_create(self):
        m = movie.Movie.create(
            title="Rambo",
        )
        # TODO: add the get to assert this test
        # self.assertEqual(m, movie.Movie.get())
        self.assertTrue(m.title == "Rambo")
        self.assertRaises(movie.TitleRequired, lambda: movie.Movie.create(title=None))

    def test_fetch(self):
        movie.Movie.create(
            title="Title test create 1"
        )
        movie.Movie.create(
            title="Title test create 2"
        )
        movies = movie.Movie.fetch()
        self.assertEqual(len(movies), 2)

    def test_check_movie_table(self):
        movie.Movie.create(
            title="StartUp"
        )
        m = movie.Movie.check_movie_table()
        self.assertEqual(m, True)

    def test_check_movie_table_empty(self):
        m = movie.Movie.check_movie_table()
        self.assertEqual(m, False)


class TestMovieApi(test.TestCase):

    def test_create(self):
        user = self.api_client.post("user.create", dict(email="test@gmail.com", password="test"))
        access_token = user.get("access_token")
        resp = self.api_client.post(
            "movie.create",
            dict(title="Terminator"),
            headers=dict(authorization=access_token)
        )

        self.assertEqual(resp["title"], "Terminator")
        resp = self.api_client.post(
            "movie.get",
            dict(title="Terminator"),
            headers=dict(authorization=access_token)
        )
        self.assertEqual(resp["title"], "Terminator")
        self.assertEqual(resp["year"], "1991")
        self.assertEqual(resp["type"], "movie")

    def test_create_fail(self):
        user = self.api_client.post("user.create", dict(email="test@gmail.com", password="test"))
        access_token = user.get("access_token")
        resp = self.api_client.post(
            "movie.create",
            dict(),
            headers=dict(authorization=access_token)
        )
        self.assertEqual(resp["error"]["error_name"], "TitleRequired")
        self.assertEqual(resp["error"]["message"], "The title field is required")

    def test_fetch(self):
        user = self.api_client.post("user.create", dict(email="test@gmail.com", password="test"))
        access_token = user.get("access_token")
        self.create_movies_to_fetch(access_token)
        
        resp_one = self.api_client.post(
            "movie.fetch",
            dict(limit=1),
            headers=dict(authorization=access_token)
        )
        self.assertEqual(len(resp_one["movies"]), 1)
        
        resp_three = self.api_client.post(
            "movie.fetch",
            dict(limit=3),
            headers=dict(authorization=access_token)
        )
        self.assertEqual(len(resp_three["movies"]), 3)

        resp_three = self.api_client.post(
            "movie.fetch",
            dict(),
            headers=dict(authorization=access_token)
        )
        self.assertEqual(len(resp_three["movies"]), 10)

    def test_get(self):
        user = self.api_client.post("user.create", dict(email="test@gmail.com", password="test"))
        access_token = user.get("access_token")
        resp_create = self.api_client.post(
            "movie.create",
            dict(title="A Clockwork Orange"),
            headers=dict(authorization=access_token)
        )
        self.assertEqual(resp_create["title"], "A Clockwork Orange")
        resp = self.api_client.post(
            "movie.get",
            dict(title="A Clockwork Orange"),
            headers=dict(authorization=access_token)
        )
        
        self.assertEqual(resp["title"], "A Clockwork Orange")
        self.assertEqual(resp["year"], "1971")
        self.assertEqual(resp["imdbid"], "tt0066921")
        self.assertEqual(resp["type"], "movie")

    def test_delete(self):
        user = self.api_client.post("user.create", dict(email="test@gmail.com", password="test"))
        access_token = user.get("access_token")
        resp_create = self.api_client.post(
            "movie.create",
            dict(title="Hobbit"),
            headers=dict(authorization=access_token)
        )
        self.assertEqual(resp_create["title"], "Hobbit")
        resp = self.api_client.post(
            "movie.delete",
            dict(id=3),
            headers=dict(authorization=access_token)
        )
        self.assertEqual(resp["deleted"], True)
        resp_fail = self.api_client.post(
            "movie.delete",
            dict(id=1),
            headers=dict(authorization=access_token)
        )
        self.assertEqual(resp_fail["deleted"], False)

    def create_movies_to_fetch(self, access_token):
        self.api_client.post(
            "movie.create",
            dict(title="The Lord of the Ring"),
            headers=dict(authorization=access_token)
        )
        self.api_client.post(
            "movie.create",
            dict(title="Batman"),
            headers=dict(authorization=access_token)
        )
        self.api_client.post(
            "movie.create",
            dict(title="Star Wars"),
            headers=dict(authorization=access_token)
        )
        self.api_client.post(
            "movie.create",
            dict(title="Doctor Strange"),
            headers=dict(authorization=access_token)
        )
        self.api_client.post(
            "movie.create",
            dict(title="Everything Everywhere All at Once"),
            headers=dict(authorization=access_token)
        )
        self.api_client.post(
            "movie.create",
            dict(title="The Northman"),
            headers=dict(authorization=access_token)
        )
        self.api_client.post(
            "movie.create",
            dict(title="Spider-Man"),
            headers=dict(authorization=access_token)
        )
        self.api_client.post(
            "movie.create",
            dict(title="Citizen Kane"),
            headers=dict(authorization=access_token)
        )
        self.api_client.post(
            "movie.create",
            dict(title="The Godfather"),
            headers=dict(authorization=access_token)
        )
        self.api_client.post(
            "movie.create",
            dict(title="Rear Window"),
            headers=dict(authorization=access_token)
        )
        self.api_client.post(
            "movie.create",
            dict(title="Boyhood"),
            headers=dict(authorization=access_token)
        )
        self.api_client.post(
            "movie.create",
            dict(title="Boyhood"),
            headers=dict(authorization=access_token)
        )
