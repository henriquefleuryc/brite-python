
import asyncio

from backend.client import omdb_transport
from backend.wsgi import remote, messages, message_types
from backend import api, movie
from backend.swagger import swagger
from backend.oauth2 import oauth2

class MovieRequestList(messages.Message):
    limit = messages.IntegerField(1, default=10)
    cursor = messages.StringField(2, default=None)

class MovieResult(messages.Message):
    title   = messages.StringField(1)
    year    = messages.StringField(2)
    imdbid  = messages.StringField(3)
    type    = messages.StringField(4)

class MovieListResponse(messages.Message):
    movies = messages.MessageField(MovieResult, 1, repeated=True)
    next_cursor = messages.StringField(2, default=None)

class MovieRequest(messages.Message):
    title = messages.StringField(1)

class MovieDelete(messages.Message):
    id = messages.IntegerField(1)

class MovieDeleteResponse(messages.Message):
    deleted = messages.BooleanField(1)

@api.endpoint(path="movie", title="Movie API")
class Movie(remote.Service):
    """ The movie API class."""
    
    @swagger("Fetch Movies")
    @oauth2.required()
    @remote.method(MovieRequestList, MovieListResponse)
    def fetch(self, request):
        movies = []
        movies, next_cursor = movie.Movie.fetch(request.limit, request.cursor)
        mr =  MovieListResponse(movies=[MovieResult(
            title=m.title,
            year=m.year,
            imdbid=m.imdbid,
            type=m.type
        ) for m in movies if m is not None])
        mr.next_cursor = next_cursor
        return mr

    @swagger("Check Movie Table")
    @oauth2.required()
    @remote.method(message_types.VoidMessage, MovieResult)
    def check_movie_table(self, request):
        #TODO FIX IT
        m = movie.Movie.check_movie_table()
        return MovieResult(
            title=m.title,
            year=m.year,
            imdbid=m.imdbid,
            type=m.type
        )

    @swagger("Create Movie")
    @oauth2.required()
    @remote.method(MovieRequest, MovieResult)
    def create(self, request):
        m = movie.Movie.create(
            title=request.title
        )
        
        omdb_api = omdb_transport.OmdbTransport()
        # loop = asyncio.get_event_loop()
        # loop.create_task(omdb_api.get_movie_details(m))
        r = omdb_api.get_movie(m)

        return MovieResult(
            title   = r.title,
            year    = r.year,
            imdbid  = r.imdbid,
            type    = r.type
        )

    @swagger("Get Movie")
    @oauth2.required()
    @remote.method(MovieRequest, MovieResult)
    def get(self, request):
        if request.title:
            m = movie.Movie.get(title=request.title)
        else:
            m = movie.Movie.get()

        rm = MovieResult(
            title=m.title,
            year=m.year,
            imdbid=m.imdbid,
            type=m.type
        )

        return rm

    @swagger("Delete Movie")
    @oauth2.required()
    @remote.method(MovieDelete, MovieDeleteResponse)
    def delete(self, request):
        r = movie.Movie.delete(request.id)
        return MovieDeleteResponse(
            deleted = r
        )
            
