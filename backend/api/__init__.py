import pkgutil
import asyncio

from backend import wsgi
from backend import movie
from backend.movie import Movie
from backend.client.omdb_transport import OmdbTransport


class Application(wsgi.Application):
    pass


application = Application(base_path="api")
service = application.service
endpoint = application.service
#TODO create a task create_task()
# movies = Movie.check_movie_table()
# print(movies)
# if movies is None:
#     loop = asyncio.get_event_loop()
#     for page in range(1, 10):
#         loop.run_until_complete(OmdbTransport.fetch_db_movies(page))


for _, modname, _ in pkgutil.walk_packages(path=pkgutil.extend_path(__path__, __name__), prefix=__name__ + '.'):
    __import__(modname)
