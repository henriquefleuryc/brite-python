from re import M
import aiohttp
import requests

from backend.movie import Movie
from backend import error

class NotFound(error.Error):
    pass


class OmdbTransport:
    service_url =  "http://www.omdbapi.com/?apikey=539f29a8"
    
    async def get_movies_async(self, page: int) -> list:
        omdb_url = self.service_url + "&s=true&type=movie&p=" + str(page)
        async with aiohttp.ClientSession() as session:
            async with session.get(omdb_url) as response:
                if response.status == 200:
                    json_rsp =  await response.json()
                    movies = [dict((k.lower(), v) for k, v in movies.items()) for movies in json_rsp['Search']]
                    movies_list = [Movie(**movie) for movie in movies]
                    return movies_list
                elif response.status >= 400:
                    raise Exception("ERROR in seach_quantity method")

    # async def get_movie_details(self, m) -> Movie:
    #     omdb_url = self.service_url + "&t=" + self.replace_title(m.title)
    #     async with aiohttp.ClientSession() as session:
    #         async with session.get(omdb_url) as response:
    #             if response.status == 200:
    #                 json_rsp =  await response.json()

    #                 # Case not find the movie in OMDB API
    #                 if "Response" in json_rsp and "Error" in json_rsp:
    #                     return Movie()

    #                 m.title=json_rsp["Title"],
    #                 m.year=json_rsp["Year"],
    #                 m.imdbid=json_rsp["imdbID"],
    #                 m.poster=json_rsp["Poster"],
    #                 m.type=json_rsp["Type"],

    #                 m.put()
    #                 return m
    #             elif response.status >= 400:
    #                 raise NotFound("Movie title %s not found on OMDB", m.title)

    def get_movie(self, m) -> Movie:
        omdb_url = self.service_url + "&t=" + self.replace_title(m.title)
        r = requests.post(omdb_url)
        if r.status_code == 200:
            json_rsp =  r.json()

            # Case not find the movie in OMDB API
            if "Response" in json_rsp and "Error" in json_rsp:
                return Movie()
            

            m.year=json_rsp["Year"]
            m.imdbid=json_rsp["imdbID"]
            m.poster=json_rsp["Poster"]
            m.type=json_rsp["Type"]

            m.put()
            return m
        elif r.status_code == 400:
            raise NotFound("Movie title %s not found on OMDB", title)

    def replace_title(self, title: str) -> str:
        return title.replace(" ", "+")

