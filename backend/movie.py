import datetime
from google.cloud import ndb

from backend import error


class NotFound(error.Error):
    pass

class TitleRequired(error.Error):
    pass

class Movie(ndb.Model):
    title   = ndb.StringProperty(indexed=True)
    year    = ndb.StringProperty()
    imdbid  = ndb.StringProperty()
    poster  = ndb.StringProperty()
    type    = ndb.StringProperty()
    created = ndb.DateTimeProperty()

    @classmethod
    def create(cls, title: str):
        if title is None:
            raise TitleRequired("The title field is required")

        entity = cls(
            title=title,
            created=datetime.datetime.now()
        )

        try:
            entity.put()
        except:
            pass

        return entity

    @classmethod
    def fetch(cls, limit=10, cursor=None):
        entity = None
        c = ndb.Cursor(cursor=cursor)

        if limit <= 0:
            limit = 10

        try:
            entity, next_cursor, more = cls.query().fetch_page(limit=limit, start_cursor=c)
            if more:
                next_cursor = next_cursor.urlsafe()
            else:
                next_cursor = None
        except:
            pass

        return entity, next_cursor

    @classmethod
    def check_movie_table(cls):
        entity = cls.query().fetch(1)
        if not entity:
            return False
        return True

    @classmethod
    def get(cls, title=None):
        if title:
            entity = cls.query(cls.title == title).fetch(1)[0]
        else:
            entity = cls.query().fetch(1).order(-cls.create)

        return entity

    @classmethod
    def delete(cls, id):
        entity = cls.get_by_id(id)
        if entity:
            entity.key.delete()
            return True
        else:
            return False
